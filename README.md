```sh
git clone git@gitlab.com:it2z-best-group/lf7/calendar.git
cd calendar
npm i -g yarn
yarn
cp .env.example .env
```

edit the .env file

```sh
yarn prisma migrate dev
yarn prisma db seed
yarn dev
```
