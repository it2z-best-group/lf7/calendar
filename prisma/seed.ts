import { PrismaClient } from "@prisma/client";
import bcrypt from "bcrypt";
const prisma = new PrismaClient();
async function main() {
  await prisma.user.create({
    data: {
      name: "admin",
      email: "admin@root.local",
      password: await bcrypt.hash("password", 8),
      isAdmin: true,
    },
  });
  await prisma.user.create({
    data: {
      name: "user",
      email: "user@root.local",
      password: await bcrypt.hash("password", 8),
    },
  });
}
main()
  .then(async () => {
    await prisma.$disconnect();
  })
  .catch(async (e) => {
    console.error(e);
    await prisma.$disconnect();
    process.exit(1);
  });
