import { credentialsRouter } from "~/server/api/routers/credentials";
import { createTRPCRouter } from "~/server/api/trpc";
import { eventsRouter } from "./routers/events";
import { usersRouter } from "./routers/users";

/**
 * This is the primary router for your server.
 *
 * All routers added in /api/routers should be manually added here.
 */
export const appRouter = createTRPCRouter({
  credentials: credentialsRouter,
  events: eventsRouter,
  users: usersRouter,
});

// export type definition of API
export type AppRouter = typeof appRouter;
