import bcrypt from "bcrypt";
import { z } from "zod";
import { adminProcedure, createTRPCRouter } from "~/server/api/trpc";

export const credentialsRouter = createTRPCRouter({
  register: adminProcedure
    .input(z.object({ name: z.string(), email: z.string(), password: z.string(), isAdmin: z.boolean() }))
    .mutation(async ({ input, ctx }) => {
      const { name, email, password: clearPassword, isAdmin } = input;
      const password = await bcrypt.hash(clearPassword, 8);
      await ctx.prisma.user.create({ data: { name, email, password, isAdmin } });
    }),
  deleteUser: adminProcedure.input(z.object({ email: z.string() })).mutation(async ({ input, ctx }) => {
    const { email } = input;
    await ctx.prisma.user.delete({ where: { email } });
  }),
});
