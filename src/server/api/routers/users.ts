import { createTRPCRouter, protectedProcedure, adminProcedure } from "~/server/api/trpc";
import z from "zod";
import bcrypt from "bcrypt";

export const usersRouter = createTRPCRouter({
  ownData: protectedProcedure.query(async ({ ctx }) => {
    const u = await ctx.prisma.user.findUnique({ where: { id: ctx.session.user.id } });
    return u;
  }),
  create: adminProcedure
    .input(z.object({ name: z.string(), password: z.string(), email: z.string(), isAdmin: z.boolean() }))
    .mutation(async ({ ctx, input }) => {
      await ctx.prisma.user.create({ data: { ...input, password: await bcrypt.hash(input.password, 8) } });
    }),
});
