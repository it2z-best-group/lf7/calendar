import type { EventCategory } from "@prisma/client";
import { TRPCError } from "@trpc/server";
import z from "zod";
import { createTRPCRouter, protectedProcedure, publicProcedure } from "~/server/api/trpc";

const categories: z.ZodType<EventCategory> = z.enum(
  Object.keys({
    MISC: null,
    MUSIC: null,
    SPORT: null,
  }) as unknown as readonly [EventCategory, ...EventCategory[]]
);

export const eventsRouter = createTRPCRouter({
  create: protectedProcedure
    .input(
      z.object({
        coordinates: z.number().array().length(2),
        event: z.string(),
        location: z.string(),
        startTime: z.string(),
        endTime: z.string(),
        category: categories,
      })
    )
    .mutation(async ({ ctx, input }) => {
      const { location, event, coordinates } = input;
      const [x, y] = coordinates;
      const { user } = ctx.session;

      if (!x || !y) throw new TRPCError({ code: "INTERNAL_SERVER_ERROR" });

      await ctx.prisma.event.create({
        data: {
          name: event,
          location: {
            connectOrCreate: {
              where: { name: location },
              create: { name: location, x, y },
            },
          },
          category: input.category,
          host: { connect: { id: user.id } },
        },
      });
    }),
  list: publicProcedure.query(async ({ ctx }) => {
    return await ctx.prisma.event.findMany({
      include: { host: { select: { name: true, email: true } }, location: true },
    });
  }),
  getUnique: publicProcedure.input(z.string()).query(async ({ ctx, input }) => {
    return await ctx.prisma.event.findUniqueOrThrow({
      where: { id: input },
      include: { host: { select: { name: true, email: true } }, location: true },
    });
  }),
});
