import { signIn, signOut, useSession } from "next-auth/react";
import type { ButtonHTMLAttributes, DetailedHTMLProps } from "react";

const AuthButton: React.FC<
  Omit<DetailedHTMLProps<ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement>, "onClick">
> = (props) => {
  const { status } = useSession();

  switch (status) {
    case "loading":
      return <button {...props}>Loading</button>;

    case "authenticated":
      return (
        <button {...props} onClick={() => void signOut()}>
          Sign Out
        </button>
      );

    case "unauthenticated":
      return (
        <button {...props} onClick={() => void signIn()}>
          Sign In
        </button>
      );
  }
};

export default AuthButton;
