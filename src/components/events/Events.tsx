import { api } from "~/utils/api";
import { UniqueEvent } from ".";

const Events: React.FC<{ eventId: string | undefined }> = () => {
  const { data: events } = api.events.list.useQuery();
  if (!events) {
    return (
      <>
        <p>Loading</p>
      </>
    );
  }
  return (
    <>
      <p>
        {events.map((e) => (
          <UniqueEvent key={e.id} event={e} />
        ))}
      </p>
    </>
  );
};

export default Events;
