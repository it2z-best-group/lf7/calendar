import type { Event, Location } from "@prisma/client";
import Link from "next/link";

const UniqueEvent: React.FC<{
  event: Event & {
    location: Location;
    host: {
      name: string;
      email: string;
    };
  };
}> = ({ event }) => {
  return (
    <div key={event.id} className="container m-2 mx-auto grid grid-cols-2 rounded border p-2">
      <label>Event Name:</label>
      <p>{event.name}</p>
      <Link className="col-span-2 grid cursor-pointer grid-cols-2" href={`/events/${event.id}`}>
        <label>Event ID:</label>
        <p>{event.id}</p>
      </Link>
      <label>Event Host Name:</label>
      <p>{event.host.name}</p>
      <label>Event Host Email:</label>
      <p>{event.host.email}</p>
      <label>Event Location Name:</label>
      <p>{event.location.name}</p>
      <Link className="col-span-2 grid cursor-pointer grid-cols-2" href={`/locations/${event.location.id}`}>
        <label>Location ID:</label>
        <p>{event.location.id}</p>
      </Link>
      <label>Event Location Coordinates:</label>
      <p>
        x={event.location.x} y={event.location.y}
      </p>
    </div>
  );
};

export default UniqueEvent;
