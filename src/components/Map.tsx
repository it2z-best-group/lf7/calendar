import Head from "next/head";
import { MapContainer, Marker, Popup, TileLayer } from "react-leaflet";

const ReactMap: React.FC<{ markers: { x: number; y: number; content: JSX.Element }[] }> = ({ markers }) => {
  return (
    <>
      <Head>
        <link rel="stylesheet" href="https://unpkg.com/leaflet@1.9.3/dist/leaflet.css" />
      </Head>
      <MapContainer
        center={[53.5003, 10.0181]}
        zoom={13}
        scrollWheelZoom={false}
        style={{
          width: "100vw",
          height: "100vh",
        }}
      >
        <TileLayer
          attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        {markers.map((marker, i) => (
          <Marker key={i} position={[marker.x, marker.y]}>
            <Popup>{marker.content}</Popup>
          </Marker>
        ))}
      </MapContainer>
    </>
  );
};

export default ReactMap;
