import dynamic from "next/dynamic";
import type { NextPage } from "next/types";
import { UniqueEvent } from "~/components/events";
import { api } from "~/utils/api";

const ReactMap = dynamic(() => import("~/components/Map"), { ssr: false });

const Home: NextPage = () => {
  const { data } = api.events.list.useQuery();

  if (!data) return <ReactMap markers={[]} />;
  return (
    <>
      {
        <ReactMap
          markers={data?.map((event) => ({
            content: <UniqueEvent event={event} />,
            x: event.location.x,
            y: event.location.y,
          }))}
        />
      }
    </>
  );
};

export default Home;
