import type { NextPage } from "next";
import { useSession } from "next-auth/react";
import Link from "next/link";
import AuthButton from "~/components/AuthButton";

const Links: React.FC = () => {
  const { data: session } = useSession();

  return (
    <ul>
      {session?.user.isAdmin ? (
        <li>
          <Link href="/register">Create User</Link>
        </li>
      ) : null}
      {session ? (
        <>
          <li>
            <Link href="/profile">Profile</Link>
          </li>
          <li>
            <Link href="/events/create">Create Event</Link>
          </li>
        </>
      ) : null}
      <li>
        <Link href="/events">Events</Link>
      </li>
    </ul>
  );
};

const Home: NextPage = () => {
  return (
    <>
      <AuthButton className="m-1 rounded border-2 border-black p-1 hover:bg-slate-300" />
      <Links />
    </>
  );
};

export default Home;
