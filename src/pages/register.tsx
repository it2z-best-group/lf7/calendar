import type { GetServerSideProps, NextPage, Redirect } from "next";
import { useState } from "react";
import { getServerAuthSession } from "~/server/auth";
import { api } from "~/utils/api";

const Register: NextPage = () => {
  const createUserMutation = api.users.create.useMutation();
  const [data, setData] = useState({
    name: "",
    email: "",
    password: "",
    isAdmin: false,
  });
  return (
    <form onSubmit={() => void createUserMutation.mutateAsync(data)}>
      <label>username</label>
      <input value={data.name} onChange={(e) => setData({ ...data, name: e.target.value })} />
      <label>email</label>
      <input value={data.email} onChange={(e) => setData({ ...data, email: e.target.value })} />
      <label>password</label>
      <input value={data.password} onChange={(e) => setData({ ...data, password: e.target.value })} />
      <label>is Admin?</label>
      <input type="checkbox" checked={data.isAdmin} onChange={() => setData({ ...data, isAdmin: !data.isAdmin })} />
      <button type="submit">Submit</button>
    </form>
  );
};

export default Register;

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const session = await getServerAuthSession(ctx);
  const obj: { props: EmptyObject; redirect?: Redirect } = { props: {} };
  if (!session?.user.isAdmin) obj.redirect = { destination: "/", permanent: false };

  return obj;
};
