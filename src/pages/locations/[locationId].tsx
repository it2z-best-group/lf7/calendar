import type { Event, Location } from "@prisma/client";
import type { GetServerSideProps, InferGetServerSidePropsType, NextPage } from "next";
import type { ParsedUrlQuery } from "querystring";
import { prisma } from "~/server/db";

const EventDetails: NextPage<{ locationData: InferGetServerSidePropsType<typeof getServerSideProps> }> = ({
  locationData,
}) => {
  return <pre>{JSON.stringify(locationData, undefined, 4)}</pre>;
};

export default EventDetails;

export const getServerSideProps: GetServerSideProps<{ locationData: Location & { events: Event[] } }> = async (ctx) => {
  const { locationId } = ctx.query as ParsedUrlQuery & { locationId: string };
  const locationData = await prisma.location.findUniqueOrThrow({
    where: { id: locationId },
    include: { events: true },
  });
  return { props: { locationData } };
};
