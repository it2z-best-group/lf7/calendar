import type { Event, Location } from "@prisma/client";
import type { GetServerSideProps, InferGetServerSidePropsType, NextPage } from "next";
import { getServerAuthSession } from "~/server/auth";
import { prisma } from "~/server/db";

const Profile: NextPage<InferGetServerSidePropsType<typeof getServerSideProps>> = ({ user }) => {
  return <pre>{JSON.stringify(user, undefined, 4)}</pre>;
};

export default Profile;

export const getServerSideProps: GetServerSideProps<{
  user?: {
    events: (Event & {
      location: Location;
    })[];
    id: string;
    name: string;
    email: string;
    isAdmin: boolean;
  };
}> = async (ctx) => {
  const session = await getServerAuthSession(ctx);
  try {
    const user = await prisma.user.findUniqueOrThrow({
      where: { id: session?.user.id },
      select: { email: true, id: true, name: true, isAdmin: true, events: { include: { location: true } } },
    });
    return { props: { user } };
  } catch {
    return { redirect: { destination: "/" }, props: {} };
  }
};
