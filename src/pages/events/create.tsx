import type { EventCategory } from "@prisma/client";
import type { GetServerSideProps, NextPage, Redirect } from "next";
import { useState } from "react";
import { getServerAuthSession } from "~/server/auth";
import { api } from "~/utils/api";

type EventData = {
  event: string;
  location: string;
  coordinates: Array<null | number>;
  startTime: string;
  endTime: string;
  category: EventCategory;
};

const EventsCreate: NextPage = () => {
  const createEvent = api.events.create.useMutation();
  const [data, setData] = useState<EventData>({
    coordinates: [null, null],
    event: "",
    location: "",
    startTime: "",
    endTime: "",
    category: "MISC",
  });
  return (
    <form
      className="container mx-auto mt-5 grid grid-cols-1 gap-2"
      onSubmit={() => void createEvent.mutateAsync(data as EventData & { coordinates: number[] })}
    >
      <label className="text-3xl">Create Event</label>
      <input
        className="rounded border p-1"
        required
        value={data.event}
        placeholder="Event Name"
        onChange={({ target }) => setData({ ...data, event: target.value })}
      />
      <input
        className="rounded border p-1"
        required
        value={data.location}
        placeholder="Location Name"
        onChange={({ target }) => setData({ ...data, location: target.value })}
      />
      <input
        className="rounded border p-1"
        type="number"
        required
        value={data.coordinates[0] || ""}
        placeholder="x"
        onChange={({ target }) =>
          setData({ ...data, coordinates: [parseFloat(target.value), data.coordinates[1] || null] })
        }
      />
      <input
        className="rounded border p-1"
        type="number"
        required
        value={data.coordinates[1] || ""}
        placeholder="y"
        onChange={({ target }) =>
          setData({ ...data, coordinates: [data.coordinates[0] || null, parseFloat(target.value)] })
        }
      />
      <div>
        <label>Start Time:</label>
        <input
          value={data.startTime}
          type="datetime-local"
          onChange={({ target }) => setData({ ...data, startTime: target.value })}
        />
      </div>
      <div>
        <label>End Time:</label>
        <input
          value={data.endTime}
          type="datetime-local"
          min={data.startTime || undefined}
          onChange={({ target }) => setData({ ...data, endTime: target.value })}
        />
      </div>
      <div>
        <label>Event Category:</label>
        <select value={data.category} onChange={(e) => setData({ ...data, category: e.target.value as EventCategory })}>
          {["SPORT", "MUSIC", "MISC"].map((cat) => (
            // ~~nyaa
            <option key={cat} value={cat}>
              {cat.toLowerCase()}
            </option>
          ))}
        </select>
      </div>
      <button className="rounded border p-1" type="submit">
        Submit
      </button>
      <pre>{JSON.stringify(data, undefined, 4)}</pre>
    </form>
  );
};

export default EventsCreate;

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const session = await getServerAuthSession(ctx);
  const obj: { props: EmptyObject; redirect?: Redirect } = { props: {} };
  if (!session) obj.redirect = { destination: "/events", permanent: false };

  return obj;
};
