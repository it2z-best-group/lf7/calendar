import { type NextPage } from "next";
import { UniqueEvent } from "~/components/events";
import { api } from "~/utils/api";

const Events: NextPage = () => {
  const { data: events } = api.events.list.useQuery();
  return (
    <>
      {events?.map((event) => (
        <UniqueEvent key={event.id} event={event} />
      ))}
    </>
  );
};

export default Events;
