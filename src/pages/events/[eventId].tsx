import type { Event, Location } from "@prisma/client";
import type { GetServerSideProps, InferGetServerSidePropsType, NextPage } from "next";
import type { ParsedUrlQuery } from "querystring";
import { UniqueEvent } from "~/components/events";
import { prisma } from "~/server/db";

const EventDetails: NextPage<InferGetServerSidePropsType<typeof getServerSideProps>> = ({ eventData }) => {
  return <UniqueEvent event={eventData} />;
};

export default EventDetails;

export const getServerSideProps: GetServerSideProps<{
  eventData: Event & {
    location: Location;
    host: {
      id: string;
      name: string;
      email: string;
      isAdmin: boolean;
    };
  };
}> = async (ctx) => {
  const { eventId } = ctx.query as ParsedUrlQuery & { eventId: string };
  const eventData = await prisma.event.findUniqueOrThrow({
    where: { id: eventId },
    include: { host: { select: { name: true, email: true, id: true, isAdmin: true } }, location: true },
  });
  return { props: { eventData } };
};
